#!/usr/bin/env python

from __future__ import print_function
import cgitb; cgitb.enable()
import json, os
import cgi, sys, subprocess
from PIL import Image 
from cStringIO import StringIO
from cgiargparse import ArgumentParser
from math import floor, ceil
# from sh import inkscape
from random import choice, shuffle, randint

from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.utils import ImageReader
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import Paragraph
from reportlab.pdfbase import pdfmetrics
from reportlab.lib.colors import white, black, HexColor




def afm_font_name(path):
    "Extract a font name from an AFM file."
    with open(path) as f:
        for line in f:
            if line.startswith("FontName "):
                return line[9:].rstrip()

def inkscape_to_png (path):
    pngdata = subprocess.check_output(["inkscape", path, "--export-png=/dev/stdout"], stderr=subprocess.PIPE)
    pos = pngdata.index("/dev/stdout\n")
    pngdata = pngdata[pos+12:]
    return pngdata

def pil_image_from_data (data):
    return Image.open(StringIO(data))


def draw_layer(base, path):
    W, H = base.size
    if type(path) == str:
        im = Image.open(path)
    else:
        im = path

    if im.mode != "RGBA":
        im = im.convert("RGBA")
    im.thumbnail((W,H))
    w, h = im.size
    ix = (W-w)/2
    iy = (H-h)/2
    # bim = Image.new("RGB", (W,H), color=args.background)
    base.paste(im, (ix, iy), mask=im)






p = ArgumentParser("")
p.add_argument("--width", type=int, default=640, help="image scale width, default: 640")
p.add_argument("--height", type=int, default=480, help="image scale height, default: 480")
p.add_argument("--background", default="black", help="background color, default: black")
p.add_argument("--output", default="output.pdf", help="output page filename, default: output.pdf")
p.add_argument("--rows", type=int, default=5, help="number of rows of images per page")
p.add_argument("--cols", type=int, default=3, help="number of columns of images per page")
p.add_argument("--basepath", default="/var/www/vhosts/guttormsgaard.activearchives.org/httpdocs/orderings", help="where are the images really, default: .")
p.add_argument("--border", type=float, default=0.5, help="border size in cm, default: 1.0")
# p.add_argument("--font", default="Libertinage-v.afm", help="font base")
p.add_argument("--font", default="KARAN___.afm", help="font base")
p.add_argument("--fontsize", type=float, default=8.0)
p.add_argument("--leading", type=float, default=None)
p.add_argument("--padding", type=int, default=5)

args = p.parse_args()

fs = cgi.FieldStorage()
text = fs.getvalue("text")
bg = fs.getvalue("background")
if bg:
    if bg == "black":
        args.background = "black"
    elif bg == "white":
        args.background = "white"
fontsize = fs.getvalue("fontsize")
if fontsize:
    args.fontsize = float(fontsize)
leading = fs.getvalue("leading")
if leading:
    args.leading = float(leading)
else:
    args.leading = args.fontsize

if text == None:
    print ("Content-type: text/html;charset=utf-8")
    print ()
    print ("""<!DOCTYPE html>
    <html>
    <head>
        <title></title>
    </head>
    <body>
    <form action="" method="post">
    <textarea name="text" style="width: 100%; height: 480px"></textarea>
    <input type="submit" />
    </form>
    </body>
    </html>

""")
    sys.exit(0)


# calculate cell width & height based on page size, rows & cols, and border
border = args.border * cm
# print ("border", border)
image_width = (A4[0] - (border * (args.cols+1))) / args.cols
image_height = (A4[1] - (border * (args.rows+1))) / args.rows
# print ("image size", image_width, image_height)
(col, row) = (0,0)

c = canvas.Canvas(sys.stdout)

# Prepare / Embed a font
facename = afm_font_name(args.font)
fontbase = os.path.splitext(args.font)[0]
fface = pdfmetrics.EmbeddedType1Face(args.font, fontbase+".pfb")
pdfmetrics.registerTypeFace(fface)
font = pdfmetrics.Font(facename, facename, 'WinAnsiEncoding') # 'WinAnsiEncoding' 'PDFDocEncoding' 'StandardEncoding' 'MacRomanEncoding' 
pdfmetrics.registerFont(font)

# Init paragraph style
ss = getSampleStyleSheet()
if args.background == "white":
    textColor = white
    backgroundColor = black
else:
    backgroundColor = white
    textColor = black

pstyle = ParagraphStyle(name='CustomBody',
    parent=ss['BodyText'],
    leading=args.leading,
    fontSize=args.fontsize,
    fontName=facename,
    backColor=None,
    borderWidth=0,
    borderColor=None,
    borderPadding=5,
    textColor=textColor,
    spaceBefore=image_height/3,
    spaceAfter=5
)


#######
# Cell methods
#########

def draw_box (c, x, y, w, h):
    c.setStrokeColor(textColor)
    c.setFillColor(backgroundColor)
    c.rect(x,y,w,h, fill=1)

def draw_image_cell(img, canvas):
    cw = (image_width + border)
    ch = (image_height + border)
    x = border + (col*cw)
    y = border + (args.rows-row-1)*ch
    canvas.drawImage(ImageReader(img), x, y, image_width, image_height)

def draw_text_cell (text, canvas, style=pstyle, align_top=False, min_height=None):
    cw = (image_width + border)
    ch = (image_height + border)
    x = border + (col*cw)
    y = border + (args.rows-row-1)*ch
    p=Paragraph(text, pstyle)
    pp = args.padding
    w,h = p.wrap(image_width-(2*pp), 0)    # find required space

    boxheight = h + (2*pp)
    if min_height and boxheight < min_height:
        boxheight = min_height
        align_top = True

    if align_top:
        draw_box(canvas, x, y + image_height - boxheight, image_width, boxheight)
        p.drawOn(canvas, x+pp, y + image_height - h - pp)
    else:
        draw_box(canvas, x, y, image_width, boxheight)
        p.drawOn(canvas, x+pp, y+pp)

def draw_short_text (text, canvas):
    cw = (image_width + border)
    ch = (image_height + border)
    x = border + (col*cw)
    y = border + (args.rows-row-1)*ch
    pp = args.padding

    w = c.stringWidth(text, facename, args.fontsize)
    boxwidth = w + (pp*2)
    boxheight = args.leading + (pp*2)

    draw_box(canvas, x, y, boxwidth, boxheight)
    c.setStrokeColor(textColor)
    # c.setFillColorRGB(0,0,0)
    c.setFillColor(textColor)
    c.setFont(facename, args.fontsize);
    c.drawString(x+pp, y+pp, text)

def shift():
    global row, col
    col += 1
    if (col >= args.cols):
        col = 0
        row += 1
        if (row >= args.rows):
            c.showPage()
            row = 0

need_clear = False

def clear ():
    global need_clear
    if need_clear:
        shift()
    need_clear = False


for line in text.splitlines():
    line = line.strip()
    if line.startswith("./"):
        clear()
        ipath = os.path.join(args.basepath, line)
        frame = Image.new("RGBA", (args.width, args.height), color=args.background)

        if ipath.endswith(".svg"):
            pngdata = inkscape_to_png(ipath)
            ipath = pil_image_from_data(pngdata)

        draw_layer(frame, ipath)
        print ("draw_image_cell", file=sys.stderr)
        draw_image_cell(frame, c)
        need_clear = True
    elif line.startswith("caption:"):
        line = line[len("caption:"):].strip()
        draw_text_cell(line, c)
    elif line.startswith("top:"):
        line = line[len("top:"):].strip()
        draw_text_cell(line, c, align_top=True)
    elif line.startswith("short:"):
        line = line[len("short:"):].strip()
        draw_short_text(line, c)
    elif not line.startswith("#"):
        clear()
        draw_text_cell(line, c, min_height = image_height)
        need_clear = True

print ("Content-type: application/pdf")
print ()
c.save()

