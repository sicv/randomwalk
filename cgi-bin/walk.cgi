#!/usr/bin/env python

import cgi
import cgitb; cgitb.enable()
import json, os, sys
from math import floor, ceil
from random import choice, shuffle, randint

from collections import namedtuple


method = os.environ.get("REQUEST_METHOD")

fs = cgi.FieldStorage()
submit = fs.getvalue("_submit", "")

args = namedtuple("args", "index limit basepath original incremenal skip_layers min_walk max_walk")
args.index = "/var/www/vhosts/guttormsgaard.activearchives.org/httpdocs/orderings/index.json"
args.basepath = "."
args.original = True
args.incremental = False
args.min_walk = 1
args.max_walk = 10

if submit == "":
    print "Content-type:text/html;charset=utf-8"
    print 
    print """<!DOCTYPE html>
    <html>
    <head>
        <title></title>
        <style>
        input.s {
        width: 32px;
        }
        </style>
    </head>
    <body>
    <form action="" method="get">
        Limit: <input type="text" name="l" value="24" class="s" /> Walk steps, min:<input type="text" name="min" value="1" class="s" /> max:<input type="text" name="max" value="2" class="s" /><br>
        <input type="checkbox" name="skip_layers" value="skip_layers" />skip layers (don't build up layers)<br>
        <input type="submit" name="_submit" value="WALK" />
    </form>
    </body>
    </html>
    """
    sys.exit(0)

args.limit = int(fs.getvalue("l", "5"))
if args.limit > 100 or args.limit < 0:
    raise Exception("bad limit")
args.skip_layers = False
if fs.getvalue("skip_layers", ""):
    args.skip_layers = True
min_steps = fs.getvalue("min")
if min_steps:
    args.min_walk = int(min_steps)
max_steps = fs.getvalue("max")
if max_steps:
    args.max_walk = int(max_steps)



### DO THE WALK AND OUTPUT AS TEXT

print "Content-type:text/html; charset=utf-8"
print
print """
<!DOCTYPE html>
<html>
<head>
    <title>Random walk</title>
    <meta charset="utf-8">
</head>
<body>
<form method="post" action="walkpdf.cgi">
<!-- <select name="router"><option>PDF</option></select> -->
<div>
<input type="submit" name="_submit" value="make pdf" />
image background color: <select name="background">
<option>black</option>
<option>white</option>
</select>
font size:<input type="text" name="fontsize" value="8" style="width: 32px" /> leading:<input type="text" name="leading" value="" style="width: 32px" />
</div>
<textarea style="width: 640px; height: 480px" name="text">"""

with open (args.index) as f:
    data = json.load(f)

names = data.keys()
names.sort()
orderings = data[names[0]].keys()
orderings.sort()

# Created ordered lists per ordering
ordered_images = {}
for o in orderings:
    items = [x for x in data.values() if o in x and 'UserComment' in x[o]]
    items.sort(key=lambda d: d[o]['UserComment'], reverse=True)
    ordered_images[o] = items

def fixname (n):
    if n.startswith("sift/"):
        n = n + ".svg"
    elif n.startswith("contours/") or n.startswith("gradient/"):
        n = n[:-4]+".svg"
    return n

def usepath (ipath):
    ipath = fixname(ipath)
    ipath = os.path.join(args.basepath, ipath)
    # if ipath.endswith(".svg"):
    #   inkscape("-w", str(args.width), "--export-png=tmp.png", ipath)
    #   ipath = "tmp.png"
    return ipath

def nonzeroorderings (img):
    return [x for x in img.keys() if 'UserComment' in img[x] and img[x]['UserComment'] != 0]

count = 0
walkordering = None

# pick random starting image
# print "img = data[choice(names)]"
trycount = 0
while True:
    trycount += 1
    img = data[choice(names)]
    imgorderings = nonzeroorderings(img)
    if len(imgorderings) >= 8:
        break

ipath = img["preview"]['SourceFile']
ipath = usepath(ipath)
print ipath
print "caption: Consider a randomly selected image to start."

first_time = True

while count < args.limit:
    # honor walkordering if set (loop)
    imgorderings = nonzeroorderings(img)
    if walkordering:
        try:
            imgorderings.remove(walkordering)
        except ValueError:
            pass

    # construct image from random layers
    if first_time: # not args.skip_layers:
        print "An image can be represented in many ways besides just pixels."
        shuffle(imgorderings)
        for i, ro in enumerate(imgorderings):
            ipath = img[ro]['SourceFile']
            ipath = usepath(ipath)
            print ipath
            print "short:", ro

        print "Each representation produces different ways of quantifying an image (the total length of contours, the number of predominantly red pixels, and so on). Each quantification enables an ordering of the collection. In each ordering an image has different neighbours. These orderings can be used as the basis of a random walk."
        first_time = False
    elif args.original:
        ipath = img["preview"]['SourceFile']
        ipath = usepath(ipath)
        print ipath
        # print "caption: The original pixels"


    walkordering = choice(imgorderings)
    ipath = usepath(img[walkordering]['SourceFile'])
    print ipath
    # print "caption: {0}".format(walkordering)

    # WALK across
    # print "walk across"
    walkitems = ordered_images[walkordering]
    curindex = walkitems.index(img)
    print "short: {0} {1}/{2}".format(walkordering, curindex, len(walkitems))
    direction = choice((1, -1))
    number = randint(args.min_walk, args.max_walk)
    # print "direction", direction, "number", number
    for img in walkitems[curindex+direction:curindex+direction+(direction*number):direction]:
        ipath = img[walkordering]['SourceFile']
        ipath = usepath(ipath)
        print ipath
        print "short: {1}/{2}".format(walkordering, walkitems.index(img), len(walkitems))
    count += 1

print """</textarea>
</form>
</body>
</html>
"""
