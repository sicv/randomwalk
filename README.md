Code for Guttormsgaard Random Walk.

See the [live example](http://guttormsgaard.activearchives.org/cgi-bin/walk.cgi).

Dependencies
* [PIL(low)](https://pypi.python.org/pypi/Pillow)
* [ReportLab](http://reportlab.com)


	pip install pillow reportlab

